import java.util.Scanner;

public class Primenumber {
	
	   public static void main(String args[])
	   {		
		
		
		Scanner scan= new Scanner(System.in);
		System.out.println("Enter any number:");
		//capture the input in an integer
		int num=scan.nextInt();
	
	    if (num == 0||num == 1) {
	    	System.out.println(num + " is not a prime number");
	    }
	    else {
	    boolean Prime=true;
		for(int i=2;i<=num/2;i++)
		{
	        
		   if(num%i==0)
		   {
		      Prime=false;
		      break;
		   }
		}
		//If isPrime is true then the number is prime else not
		if(Prime)
		   System.out.println(num + " is a Prime Number");
		else
		   System.out.println(num + " is not a Prime Number");
	    }
	   }
	}


